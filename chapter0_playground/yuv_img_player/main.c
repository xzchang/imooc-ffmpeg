#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <string.h>
#include <getopt.h>
#include <openssl/md5.h>
#include <unistd.h>

#include "SDL.h"

#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libswresample/swresample.h"
#include "libavutil/avstring.h"

void read_file( AVPicture *pict, const char *const file, const int width, const int height) {
    FILE *pf = fopen(file, "r");
    size_t ret = 0;
    MD5_CTX md5;
    unsigned char digest[MD5_DIGEST_LENGTH];
    if (pf == NULL) {
        printf("fopen failed, errorno = %d\n", errno);
        return;
    }
    printf("width = %d, height = %d, file_to_play = %s\n", width, height, file);
    pict->linesize[0] = width;
    pict->linesize[1] = width/2;
    pict->linesize[2] = width/2;
    ret = fread(pict->data[0], width * height, 1, pf);
    if (ret != 1) {
        fprintf(stderr, "Reading component Y failed\n");
        goto recycle;
    }

    ret = fread(pict->data[1], width/2 * height/2, 1, pf);
    if (ret != 1) {
        fprintf(stderr, "Reading component U failed\n");
        goto recycle;
    }

    ret = fread(pict->data[2], width/2 * height/2, 1, pf);
    if (ret != 1) {
        fprintf(stderr, "Reading component V failed\n");
        goto recycle;
    }

    MD5_Init(&md5);
    MD5_Update(&md5, pict->data[0], width * height);
    MD5_Update(&md5, pict->data[1], width / 2 * height / 2);
    MD5_Update(&md5, pict->data[2], width / 2 * height / 2);
    MD5_Final(digest, &md5);
    printf("md5sum:\n");
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++) {
        printf("%02X", digest[i]);
    }
    printf("\n");
recycle:
    fclose(pf);
}

void __main(const char *const file, const int width, const int height) {
    SDL_Window *win;
    SDL_Renderer *renderer;
    SDL_Texture *texture;
    SDL_Rect rect;
    rect.x = rect.y = 0;
    rect.w = width;
    rect.h = height;

    AVPicture *pict = (AVPicture*) malloc(sizeof(AVPicture));
    avpicture_alloc(pict, AV_PIX_FMT_YUV420P, width, height);
    read_file(pict, file, width, height);

    win = SDL_CreateWindow(
            "Media Player",
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            width,
            height,
            SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

    renderer = SDL_CreateRenderer(win, -1, 0);

    texture = SDL_CreateTexture(
            renderer,
            SDL_PIXELFORMAT_IYUV,
            SDL_TEXTUREACCESS_STREAMING,
            width,
            height);
    SDL_UpdateYUVTexture(
            texture,
            NULL,
            pict->data[0],
            pict->linesize[0],
            pict->data[1],
            pict->linesize[1],
            pict->data[2],
            pict->linesize[2]
            );

    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, texture, NULL, &rect);
    SDL_RenderPresent(renderer);

    sleep(10);
    avpicture_free(pict);
    free(pict);
}

int main(int argc, char* argv[]) {
    char* file_to_play = NULL;
    int width, height = -1;
    int c = -1;
    while((c = getopt(argc, argv, "i:w:h:")) != -1) {
        switch(c) {
            case 'i':
                file_to_play = optarg;
                break;
            case 'w':
                width = atoi(optarg);
                break;
            case 'h':
                height = atoi(optarg);
                break;
            default:
                exit(1);

        }
    }

    if (width == -1 || height == -1 || file_to_play == NULL) {
        fprintf(stderr, "Invalid/Missing parameter\n");
        exit(1);
    }

    __main(file_to_play, width, height);
    return 0;
} 
