#include <stdio.h>
#include "libavformat/avformat.h"
#include "libavutil/log.h"

int main(int argc, char* argv[])
{
	int ret;
	ret = avpriv_io_delete("./sample.txt");
	if (ret <0) {
		av_log(NULL, AV_LOG_ERROR, "Failed to delete file sample.txt\n");
		return -1;
	}
		
	return 0;
}
