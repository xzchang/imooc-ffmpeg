#include <stdio.h>
#include <getopt.h>
#include "libavformat/avformat.h"
#include "libavutil/log.h"

int print_metadata(char* file_dir, int stream_idx)
{
	AVFormatContext *fmt_ctx = NULL;
	int ret = 0;
	av_log_set_level(AV_LOG_INFO);

	// Deprecated method, don't need anymore
	// av_register_all();

	ret = avformat_open_input(&fmt_ctx, file_dir, NULL, NULL); 

	if (ret < 0) {
		av_log(NULL, AV_LOG_ERROR, "Can't open file: %s\n", av_err2str(ret));
		ret = -1;
		goto __fail;
	}

	av_dump_format(fmt_ctx, stream_idx, file_dir, 0);

__fail:	
	avformat_close_input(&fmt_ctx);
	return ret;
}

int main(int argc, char* argv[])
{
	char* file_dir;
	int stream_idx;
	int c;
	while ((c = getopt(argc, argv, "f:i:")) != -1) {
		switch(c) {
		case 'f':
			file_dir = optarg;
			break;
		case 'i':
			stream_idx = atof(optarg);
			break;
		}
	}
	print_metadata(file_dir, stream_idx);
}
