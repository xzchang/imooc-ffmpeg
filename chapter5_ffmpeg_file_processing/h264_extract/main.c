#include <stdio.h>
#include <getopt.h>
#include "libavformat/avformat.h"
#include "libavutil/log.h"

#ifndef AV_WB32
#   define AV_WB32(p, val) do {                 \
        uint32_t d = (val);                     \
        ((uint8_t*)(p))[3] = (d);               \
        ((uint8_t*)(p))[2] = (d)>>8;            \
        ((uint8_t*)(p))[1] = (d)>>16;           \
        ((uint8_t*)(p))[0] = (d)>>24;           \
    } while(0)
#endif

#ifndef AV_RB16
#   define AV_RB16(x)                           \
    ((((const uint8_t*)(x))[0] << 8) |          \
      ((const uint8_t*)(x))[1])
#endif

static int alloc_and_copy(AVPacket *out,
                          const uint8_t *sps_pps, uint32_t sps_pps_size,
                          const uint8_t *in, uint32_t in_size)
{
	uint32_t offset = out -> size;
	uint8_t nal_header_size = offset ? 3: 4;
	int err;

	err = av_grow_packet(out, sps_pps_size + in_size + nal_header_size);
	if (err < 0)
		return err;

	if (sps_pps)
		memcpy(out -> data + offset, sps_pps, sps_pps_size);

	memcpy(out -> data + offset + sps_pps_size + nal_header_size, in, in_size);

	if (!offset) {
		AV_WB32(out -> data + sps_pps_size, 1);
	} else {
		(out -> data + offset + sps_pps_size)[0] =
		(out -> data + offset + sps_pps_size)[1] = 0;
		(out -> data + offset + sps_pps_size)[2] = 1;
	}

	return 0;
}

int h264_extradata_to_annexb(const uint8_t *codec_extradata, const int codec_extradata_size, AVPacket *out_extradata, int padding)
{
	uint16_t unit_size = 0;
	uint64_t total_size = 0;
	uint8_t *out = NULL;
	uint8_t unit_nb;
	uint8_t sps_done = 0;
	uint8_t sps_seen = 0;
	uint8_t pps_seen = 0;
	uint8_t sps_offset = 0;
	uint8_t pps_offset = 0;
	const uint8_t *extradata = codec_extradata + 4;// Inside the codec_extradata, the first 4 bytes are useless
	static const uint8_t nalu_header[4] = {0, 0, 0, 1};
	int length_size = (*extradata++ & 0x3) + 1;// retrieve sps/pps size

	sps_offset = pps_offset = -1;

	/* Retrieve sps and pps unit(s) */
	unit_nb = *extradata++ & 0x1f; /* number of sps unit(s) */
	if (!unit_nb) {
		goto pps;
	} else {
		sps_offset = 0;
		sps_seen = 1;
	}

	while (unit_nb--) {
		int err;

		unit_size = AV_RB16(extradata);
		total_size += unit_size + 4;
		if (total_size > INT_MAX - padding) {
			av_log(NULL, AV_LOG_ERROR,
				"Too big extradata size, corrupted stream or invalid MP4/AVCC bitstream\n");
			av_free(out);
			return AVERROR(EINVAL);
		}

		if (extradata + 2 + unit_size > codec_extradata + codec_extradata_size) {
			av_log(NULL, AV_LOG_ERROR, "Packet header is not contained in global extradata, "
				"corrupted stream or invalid MP4/AVCC bitstream\n");
			av_free(out);
			return AVERROR(EINVAL);
		}

		if ((err = av_reallocp(&out, total_size + padding)) < 0)
			return err;

		memcpy(out + total_size - unit_size -4, nalu_header, 4);
		memcpy(out + total_size - unit_size, extradata + 2, unit_size);
		extradata += 2 + unit_size;

pps:
		if (!unit_nb && !sps_done++) {
			unit_nb = *extradata++; /* number of pps unit(s) */
			if (unit_nb) {
				pps_offset = total_size;
				pps_seen = 1;
			}
		}
	}

	if (out)
		memset(out + total_size, 0, padding);

	if (!sps_seen)
		av_log(NULL, AV_LOG_WARNING,
				"Warning: SPS NALU missing or invalid. "
				"The resulting stream may not play.\n");

	if (!pps_seen)
		av_log(NULL, AV_LOG_WARNING,
				"Warning: PPS NALU missing or invalid. "
				"The resulting stream may not play.\n");

	out_extradata -> data = out;
	out_extradata -> size = total_size;

	return length_size;
}

// Handle NALU
int h264_mp4toannexb(AVFormatContext* fmt_ctx, AVPacket* in, FILE *dst_fd)
{
	AVPacket* out = NULL;
	AVPacket spspps_pkt;

	int len;
	uint8_t nal_unit_type;
	int32_t nal_size;
	uint32_t cumul_size = 0;
	const uint8_t *buf;
	const uint8_t *buf_end;
	int buf_size;
	int ret = 0, i;

	out = av_packet_alloc();

	buf = in -> data;
	buf_size = in -> size;
	buf_end  = in -> data + in -> size;

	do {
		ret = AVERROR(EINVAL);
		if (buf + 4 > buf_end)
			goto fail;

		for (nal_size = 0, i = 0; i < 4; i++)
			nal_size = (nal_size << 8) | buf[i];

		if (nal_size > buf_end - buf || nal_size < 0)
			goto fail;

		buf += 4;
		// https://yumichan.net/video-processing/video-compression/introduction-to-h264-nal-unit/
		// The first byte is the NAL header
		// forbidden_zero_bit		f(1)
		// nal_ref_idc				u(2)
		// nal_unit_type			u(5)
		nal_unit_type = *buf & 0x1f;

		// nal_unit_type == 7 ==> SPS(sequence parameter set) NAL unit
		// nal_unit_type == 8 ==> PPS(picture parameter set) NAL unit
		// nal_unit_type == 5 ==> IDR frame - a special i frame
		// nal_unit_type == 1 ==> Non IDR frame
		// nal_unit_type == 9 ==> AUD(Access Delimiter Unit)
		if (nal_unit_type == 5) {//prepend SPS/PPS to every type 5 NAL unit
			h264_extradata_to_annexb(fmt_ctx -> streams[in -> stream_index] -> codec -> extradata,
									 fmt_ctx -> streams[in -> stream_index] -> codec -> extradata_size,
									 &spspps_pkt,
									 AV_INPUT_BUFFER_PADDING_SIZE);

			if ((ret = alloc_and_copy(out, spspps_pkt.data, spspps_pkt.size, buf, nal_size)) < 0)
				goto fail;
		} else {
			if ((ret = alloc_and_copy(out, NULL, 0, buf, nal_size)) < 0)
				goto fail;
		}

		len = fwrite(out -> data, 1, out -> size, dst_fd);

		if (len != out -> size) {
			av_log(NULL, AV_LOG_DEBUG, "warning, length of writed data isn't equal pkt.size(%d, %d)\n",
					len,
					out -> size);
		}
		fflush(dst_fd);


// next_nal:
		buf += nal_size;
		cumul_size += nal_size + 4;
	} while (cumul_size < buf_size);

fail:
	av_packet_free(&out);

	return ret;

}

int extract_video(char* src, char* dst)
{
	AVPacket pkt;
	AVFormatContext *fmt_ctx = NULL;
	int ret = 0;
	int video_stream_index = 0;
	av_log_set_level(AV_LOG_INFO);
	char errors[1024];
	int err_code;

	// Open output file
	FILE* dst_fd = fopen(dst, "wb");
	if (!dst_fd) {
		av_log(NULL, AV_LOG_ERROR, "Can't open output file! %s\n", dst);
		avformat_close_input(&fmt_ctx);
		return -1;
	}

	// Open input file
	if ((err_code = avformat_open_input(&fmt_ctx, src, NULL, NULL) < 0)) {
		av_strerror(err_code, errors, 1024);
		av_log(NULL,
				AV_LOG_ERROR,
				"Can't open file: %s\n, with errorCode = %d errorMessage = %s",
				av_err2str(ret),
				err_code,
				errors);
		return -1;
	}

	// Dump input file information
	av_dump_format(fmt_ctx, 0, src, 0);

	// Initialize a packet
	av_init_packet(&pkt);
	pkt.data = NULL;
	pkt.size = 0;

	// Find video stream
	video_stream_index = av_find_best_stream(fmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);

	if (video_stream_index < 0) {
		av_log(NULL, AV_LOG_DEBUG, "Can't find the video stream");
		avformat_close_input(&fmt_ctx);
		fclose(dst_fd);
		return -1;
	}

	// Loop each packet in the stream
	while (av_read_frame(fmt_ctx, &pkt) >= 0) {
		if (pkt.stream_index == video_stream_index) {
			h264_mp4toannexb(fmt_ctx, &pkt, dst_fd);
		}

		av_packet_unref(&pkt);
	}
	avformat_close_input(&fmt_ctx);
	if (dst_fd) {
		fclose(dst_fd);
	}


	av_log(NULL, AV_LOG_INFO, "Extraction success\n");
	return 1;
}

int main(int argc, char* argv[])
{
	char* src;
	char* dst;
	int c;
	while ((c = getopt(argc, argv, "i:o:")) != -1) {
		switch(c) {
			case 'i':
				src = optarg;
				break;
			case 'o':
				dst = optarg;
				break;
		}
	}
	return extract_video(src, dst);
}
