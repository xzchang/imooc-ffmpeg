#include <stdio.h>
#include <string.h>
#include <getopt.h>

#include "SDL.h"

#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libavutil/imgutils.h"

int play_video(const char *const file)
{
	int ret = -1;

	AVFormatContext *pFormatCtx = NULL; // for open multi-media file

	int videoStream;

	AVCodecContext *pCodecCtx = NULL;

	struct SwsContext *sws_ctx = NULL;

	AVCodec *pCodec = NULL;
	AVFrame *pFrame = NULL;
	AVPacket packet;

	SDL_Rect rect;
	Uint32 pixformat;

	// for render
	SDL_Window *win = NULL;
	SDL_Renderer *renderer = NULL;
	SDL_Texture *texture = NULL;

	// set default size of window
	int w_width = 640;
	int w_height = 480;

	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER)) {
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not initialize SDL - %s\n", SDL_GetError());
		return ret;
	}

	// Open video file
	if (avformat_open_input(&pFormatCtx, file, NULL, NULL) != 0) {
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Failed to open video file!");
		goto __FAIL; // Couldn't open file
	}

	// Retrieve stream information
	if (avformat_find_stream_info(pFormatCtx, NULL) < 0) {
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Failed to find stream inforamtion!");
		goto __FAIL;
	}

	// Dump information about file onto standard error
	av_dump_format(pFormatCtx, 0, file, 0);

	// Find the first video stream
	videoStream = av_find_best_stream(pFormatCtx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);

	if (videoStream < 0) {
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Didn't find a video stream!");
		goto __FAIL;
	}

	// Find the proper decoder
	pCodec = avcodec_find_decoder(pFormatCtx->streams[videoStream]->codecpar->codec_id);
	if (!pCodec) {
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Unsupported codec!\n");
		goto __FAIL;
	}

	pCodecCtx = avcodec_alloc_context3(pCodec);
	// Copy context
	if (avcodec_parameters_to_context(pCodecCtx, pFormatCtx->streams[videoStream]->codecpar) != 0) {
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Couldn't copy codec context");
		goto __FAIL;
	}

	// Open codec
	if (avcodec_open2(pCodecCtx, pCodec, NULL) < 0) {
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Failed to open decoder!\n");
		goto __FAIL;
	}

	// Allocate video frme
	pFrame = av_frame_alloc();

	w_width = pCodecCtx -> width;
	w_height = pCodecCtx -> height;

	win = SDL_CreateWindow("Media Player",
			SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			w_width,
			w_height,
			SDL_WINDOW_OPENGL |SDL_WINDOW_RESIZABLE);

	if (!win) {
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Failed to create window by SDL");
		goto __FAIL;
	}

	renderer = SDL_CreateRenderer(win, -1, 0);
	if (!renderer) {
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Failed to create Renderer by SDL");
		goto __FAIL;
	}

	pixformat = SDL_PIXELFORMAT_IYUV;
	texture = SDL_CreateTexture(renderer,
			pixformat,
			SDL_TEXTUREACCESS_STREAMING,
			w_width,
			w_height);

	// initialize SWS context for software scaling
	sws_ctx = sws_getContext(pCodecCtx->width,
			pCodecCtx->height,
			pCodecCtx->pix_fmt,
			pCodecCtx->width,
			pCodecCtx->height,
			AV_PIX_FMT_YUV420P,
			SWS_BILINEAR,
			NULL,
			NULL,
			NULL);

	ret = av_image_alloc(pFrame->data, pFrame->linesize, pCodecCtx->width, pCodecCtx->height, pCodecCtx->pix_fmt, 32);
	if (ret < 0) {
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Unable to allocate raw picture buffer\n");
		goto __FAIL;
	}

	// Read frames and save first five frames to disk
	while(av_read_frame(pFormatCtx, &packet) >= 0) {
		// Is this a packet from the video stream?
		if (packet.stream_index == videoStream) {

			ret = avcodec_send_packet(pCodecCtx, &packet);
			if (ret < 0) {
				SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Unable to send packet to codec");
				goto __FAIL;
			}

			while (ret >= 0) {
				ret = avcodec_receive_frame(pCodecCtx, pFrame);

				if (ret == AVERROR(EAGAIN)) {
					// Per the doc, we need to feed more data into the codec
					// I like this API over the older one
					break;
				} else if (ret < 0) {
					SDL_LogError(
							SDL_LOG_CATEGORY_APPLICATION,
							"Unable to receive a frame from the codec, with errorCode = %d, errorMessage = %s",
							ret,
							av_err2str(ret));
					goto __FAIL;
				}


				// Convert the image into YUV format that SDL uses
				sws_scale(sws_ctx,
						(uint8_t const* const*)pFrame->data,
						pFrame->linesize,
						0,
						pCodecCtx->height,
						pFrame->data,
						pFrame->linesize);

				SDL_UpdateYUVTexture(texture,
						NULL,
						pFrame->data[0],
						pFrame->linesize[0],
						pFrame->data[1],
						pFrame->linesize[1],
						pFrame->data[2],
						pFrame->linesize[2]);

				// Set size of window
				rect.x = 0;
				rect.y = 0;
				rect.w = pCodecCtx->width;
				rect.h = pCodecCtx->height;

				SDL_RenderClear(renderer);
				SDL_RenderCopy(renderer, texture, NULL, &rect);
				SDL_RenderPresent(renderer);
			}
		}

		// Free the packet that was allocated by av_read_frame
		av_packet_unref(&packet);

		SDL_Event event;
		SDL_PollEvent(&event);
		switch(event.type) {
			case SDL_QUIT:
				goto __QUIT;
				break;
			default:
				break;
		}

	}

__QUIT:
	ret = 0;

__FAIL:
	// Free the YUV frame
	if (pFrame) {
		av_frame_free(&pFrame);
	}

	// Close the codec
	if (pCodecCtx) {
		avcodec_close(pCodecCtx);
	}

	// Close the video file
	if (pFormatCtx) {
		avformat_close_input(&pFormatCtx);
	}

	if (win) {
		SDL_DestroyWindow(win);
	}

	if (renderer) {
		SDL_DestroyRenderer(renderer);
	}

	if (texture) {
		SDL_DestroyTexture(texture);
	}

	SDL_Quit();

	return ret;
}

int main(int argc, char* argv[])
{
	char* file_to_play = NULL;
	int c;
	while ((c = getopt(argc, argv, "i:")) != -1) {
		switch(c) {
			case 'i':
				file_to_play = optarg;
				break;
		}
	}
	if (!file_to_play) {
		fprintf(stderr, "MM file not specified!\n");
		return 0;
	}
	return play_video(file_to_play);
}
