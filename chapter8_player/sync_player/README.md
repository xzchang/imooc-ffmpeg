# How to play a video
./a.out -i ../../killer.mp4

# One of the byproduct produced while playing the video is the testout.pcm file
# We can apply the folowing to play out the video
# s16le means Signed 16 bit, Little Endian. Since we are working on a x86_64 platform. This is correct for now. Let's try it out on rpi4, which is a small endian platform
ffplay -f s16le -ar 44100 -ac 2 ~/git-root/imooc-ffmpeg/chapter8_player/decent_player/testout.pcm

# The other byprodct is the .data file. They are raw pixel file 8 bit grayscale.
# Now I'm using http://rawpixels.net/ to open it.
# Regarding the dicussion about how to save raw yuv/pixel file. It's here 
# https://stackoverflow.com/questions/35942358/saving-raw-yuv420p-frame-ffmpeg-libav/35942480#35942480

# Use the following command to play the yuv images
./ffplay -f rawvideo -pixel_format yuv420p -video_size 608x368 -i ../imooc-ffmpeg/chapter8_player/decent_player/frame-00000000_yuv420_608x368.data
