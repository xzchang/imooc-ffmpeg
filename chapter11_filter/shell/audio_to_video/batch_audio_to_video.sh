#/bin/sh
ffmpeg -i input.mp3 -filter_complex "[0:a]avectorscope=s=1920x1080,format=yuv420p[v]" -map "[v]" -map 0:a avectorscope.mp4

ffmpeg -i input.mp3 -filter_complex "[0:a]showcqt=s=1920x1080,format=yuv420p[v]" -map "[v]" -map 0:a showcqt.mp4

ffmpeg -i input.mp3 -filter_complex "[0:a]ahistogram=s=1920x1080,format=yuv420p[v]" -map "[v]" -map 0:a ahistogram.mp4

ffmpeg -i input.mp3 -filter_complex "[0:a]aphasemeter=s=1920x1080:mpc=cyan,format=yuv420p[v]" -map "[v]" -map 0:a aphasemeter.mp4

ffmpeg -i input.mp3 -filter_complex "[0:a]showfreqs=s=1920x1080:mode=line:fscale=log,format=yuv420p[v]" -map "[v]" -map 0:a showfreqs.mp4

ffmpeg -i input.mp3 -filter_complex "[0:a]showspectrum=s=1920x1080,format=yuv420p[v]" -map "[v]" -map 0:a showspectrum.mp4

ffmpeg -i input.mp3 -filter_complex "[0:a]showwaves=s=1920x1080:mode=line:rate=25,format=yuv420p[v]" -map "[v]" -map 0:a showwaves.mp4

ffmpeg -i input.mp3 -filter_complex "[0:a]showvolume=f=0.5:c=VOLUME:b=4:w=1920:h=900,format=yuv420p[v]" -map "[v]" -map 0:a showvolume.mp4
