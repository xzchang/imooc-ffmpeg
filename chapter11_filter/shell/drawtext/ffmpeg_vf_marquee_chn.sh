#/bin/sh
ffmpeg -y -t 10 -i nrk-tid-70-noCA-downscaled.mp4 -vf "drawtext=textfile=watermark_subject.txt:fontfile=SourceHanSerifSC-Regular.otf:y=h-line_h-10:x='w-w/3*mod(t,3*(w+tw)/w)':fontcolor=ffcc00:fontsize=40:shadowx=2:shadowy=2" nrk-tid-70-noCA-watermark.mp4
