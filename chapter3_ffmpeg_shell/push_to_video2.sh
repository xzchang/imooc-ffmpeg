#/bin/sh
# Step 1 Download v4l2loopback, compile and install, then you will get a video playback device /dev/video2

################################################################################################################
# sudo apt-get install build-essential checkinstall
# wget https://github.com/umlaeute/v4l2loopback/archive/master.zip
# unzip master.zip
# cd v4l2loopback-master
# make
# sudo checkinstall --pkgname=v4l2loopback --pkgversion="$(date +%Y%m%d%H%M)-git" --default
# sudo modprobe v4l2loopback
################################################################################################################
# Step 2.a push a video file into the loopbackdevice 
ffmpeg -re -i ~/git-root/imooc-ffmpeg/killer.mp4 -map 0:v -f v4l2 /dev/video2

# Step 2.b push the desktop capture to the loopbackdevice
# Here's a tutorial https://trac.ffmpeg.org/wiki/Capture/Desktop
# https://superuser.com/questions/411897/using-desktop-as-fake-webcam-on-linux/713100#713100
ffmpeg -f x11grab -framerate 15 -probesize 42M -video_size 1280x720 -i :0  -f v4l2 -pix_fmt yuv420p /dev/video2

# combine desktop & camera and output to /dev/video2
ffmpeg -thread_queue_size 512 -f x11grab -video_size 1920x1080 -framerate 30 -i :0        -f v4l2 -framerate 30 -video_size 640x360 -i /dev/video0        -filter_complex "[0][1]overlay=x=W-w:y=H-h"        -f v4l2 -pix_fmt yuv420p /dev/video2

# play video2 device
ffplay -f v4l2 /dev/video2
