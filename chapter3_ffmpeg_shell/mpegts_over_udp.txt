# Extract specific program from a mpeg ts
ffmpeg -i NRK-TID70-noCA.ts -codec copy -map 0:p:1039 NRK-TID70-noCA-output.ts -copy_unknown

# Examine audio volume
ffmpeg -i NRK-TID70-noCA-output.ts -filter:a volumedetect -f null /dev/null

# Broadcast the MPEG-TS tream through UDP protocol to simulate an IPTV behavior
ffmpeg -re -stream_loop -1  -i ~/Desktop/NRK-TID70-noCA-output.ts -c copy -f mpegts udp://239.0.0.1:8080?ttl=1

# Transcode the stream and do some optimization
ffmpeg -re -stream_loop -1  -i ~/Desktop/NRK-TID70-noCA-output.ts -preset ultrafast -vcodec libx264 -tune film -maxrate 8192k -g 120 -bufsize 64m  -f mpegts udp://239.0.0.1:8080?ttl=1&&pkt_size=1316

# Apply hardware acceleration
ffmpeg -init_hw_device vaapi=foo:/dev/dri/renderD128 -hwaccel vaapi  -hwaccel_device foo  -re  -stream_loop -1   -i ~/Desktop/NRK-TID70-noCA-output.ts -preset veryslow -c:v h264_vaapi -vf 'format=nv12,hwupload'  -tune film -maxrate 8192k -g 120 -bufsize 64m  -f mpegts udp://239.0.0.1:8080?ttl=1&&pkt_size=1316


# Apply RPI specific h264 HW acceleration
ffmpeg  -re  -stream_loop -1   -i ./NRK-TID70-noCA-output.ts  -preset fast -c:v h264_v4l2m2m  -tune film -maxrate 16M -b 8M -g 120 -bufsize 64m   -c:a copy  -f mpegts udp://239.0.0.1:8080?ttl=1&&pkt_size=1316


