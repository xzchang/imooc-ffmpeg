# install video4linux2
sudo apt-get install libv4l-dev
sudo apt-get install v4l-utils

# configure ffmpeg with v4l2 options
./configure --enable-libv4l2 --enable-zlib --enable-vaapi --enable-pthreads --enable-vdpau --enable-xlib --enable-libv4l2
# List camera features
ffplay -f video4linux2 -list_formats all /dev/video0
# Capture video in C270 in yuv mode
ffplay -f video4linux2 -framerate 30 -video_size hd720 /dev/video0
# Capture vidoe in motion jpeg mode
ffplay -f video4linux2 -framerate 30 -video_size hd720 -input_format mjpeg   /dev/video0
