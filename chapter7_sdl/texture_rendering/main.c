#include "SDL.h"
#include <stdio.h>

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480

int main(int argc, char* args[]) {
	int quit = 1;
	SDL_Window* window = NULL;
	SDL_Surface* screenSurface = NULL;
	SDL_Renderer *render = NULL;
	SDL_Texture *texture;

	SDL_Rect rect;
	rect.w = 30;
	rect.h = 30;

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "could no tinitialize sdl2 :%s\n", SDL_GetError());
		return 1;
	}

	window = SDL_CreateWindow(
								"hello_sdl2",
								SDL_WINDOWPOS_UNDEFINED,
								SDL_WINDOWPOS_UNDEFINED,
								SCREEN_WIDTH,
								SCREEN_HEIGHT,
								SDL_WINDOW_SHOWN
			);

	if (!window) {
		fprintf(stderr, "could not create window: %s\n", SDL_GetError());
		goto __EXIT;
	}

	render = SDL_CreateRenderer(window, -1, 0);

	if (!render) {
		SDL_Log("Failed to Create Render!");
		goto __DWINDOW;
	}

	texture = SDL_CreateTexture(render, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, SCREEN_WIDTH, SCREEN_HEIGHT);

	if (!texture) {
		SDL_Log("Failed to Create Texture!\n");
		goto __DRENDER;
	}
	do {
		SDL_Event event;
		SDL_PollEvent(&event);
		switch(event.type) {
			case SDL_QUIT:
				quit = 0;
			default:
				SDL_Log("event type is %d", event.type);
		}

		rect.x = rand() % 600;
		rect.y = rand() % 450;

		SDL_SetRenderTarget(render, texture);
		SDL_SetRenderDrawColor(render, 0, 0, 0, 0);
		SDL_RenderClear(render);

		SDL_RenderDrawRect(render, &rect);
		SDL_SetRenderDrawColor(render, 255, 0, 0, 0);
		SDL_RenderFillRect(render, &rect);

		// Offload the window rendering task to GPU
		SDL_SetRenderTarget(render, NULL);
		SDL_RenderCopy(render, texture, NULL, NULL);

		SDL_RenderPresent(render);
	} while(quit);

	SDL_DestroyTexture(texture);

__DRENDER:
	SDL_DestroyRenderer(render);

__DWINDOW:
	SDL_DestroyWindow(window);

__EXIT:
	SDL_Quit();
	return 0;
}
