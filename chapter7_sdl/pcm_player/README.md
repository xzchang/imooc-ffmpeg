#/bin/sh
# decode killer.mp4 audio stream as PCM
ffmpeg -i ../../killer.mp4 -acodec pcm_s16le -f s16le  killer.pcm
# play PCM file with specific properties
# -ar = sampling rate
# -ac = audio channel number
ffplay -f s16le killer.pcm -ar 44100 -ac 2

# use the compiled executable to play pcm file
./a.out -i killer.pcm
