#include "SDL.h"
#include <stdio.h>

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480

int main(int argc, char* args[]) {
	int quit = 1;
	SDL_Window* window = NULL;
	SDL_Surface* screenSurface = NULL;
	SDL_Renderer *render = NULL;

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "could no tinitialize sdl2 :%s\n", SDL_GetError());
		return 1;
	}

	window = SDL_CreateWindow(
								"hello_sdl2",
								SDL_WINDOWPOS_UNDEFINED,
								SDL_WINDOWPOS_UNDEFINED,
								SCREEN_WIDTH,
								SCREEN_HEIGHT,
								SDL_WINDOW_SHOWN
			);

	if (!window) {
		fprintf(stderr, "could not create window: %s\n", SDL_GetError());
		goto __EXIT;
	}

	render = SDL_CreateRenderer(window, -1, 0);

	if (!render) {
		SDL_Log("Failed to Create Render!");
		goto __DWINDOW;
	}

	SDL_SetRenderDrawColor(render, 255, 0, 0, 255);

	SDL_RenderClear(render);

	SDL_RenderPresent(render);

	do {
		SDL_Event event;
		SDL_WaitEvent(&event);
		switch(event.type) {
			case SDL_QUIT:
				quit = 0;
			default:
				SDL_Log("event type is %d", event.type);
		}
	} while(quit);

__DWINDOW:
	SDL_DestroyWindow(window);

__EXIT:
	SDL_Quit();
	return 0;
}
